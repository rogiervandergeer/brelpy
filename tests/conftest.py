from pytest import fixture


@fixture(scope="function")
def mocked_socket(mocker):
    socket = mocker.patch("brelpy.socket.socket")

    class FakeSocket:
        def __init__(self):
            self.responses = []
            self.sent = []
            self.closed = False

        def add_response(self, message):
            self.responses.append(message)

        def recvfrom(self, buffer: int):
            try:
                return self.responses.pop(0).encode("utf-8"), None
            except IndexError:
                raise RuntimeError("Error in test: no more responses")

        def sendto(self, message, *args):
            self.sent.append(message.decode("utf-8"))

        def settimeout(self, timeout: int):
            pass

        def close(self):
            self.closed = True

    fake_socket = FakeSocket()
    socket.return_value = fake_socket
    return fake_socket
