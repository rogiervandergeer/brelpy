from brelpy import BrelHub, BrelDevice
from brelpy.socket import BrelSocket
from brelpy.exceptions import NoKeyException, NotConnectedException
from pytest import fixture, raises


class TestHub:
    @fixture(scope="function")
    def hub(self, mocked_socket):
        mocked_socket.add_response(
            '{"msgType": "GetDeviceListAck", "token": "37412C478E0FBEAB",'
            '"data": [{"mac": "abcdefgh", "deviceType": "02000001"}, '
            '{"mac": "abcdefgh001", "deviceType": "10000000"}]}'
        )
        return BrelHub("dummy", key="74ae544c-d16e-4c")

    def test_connect(self, hub, mocked_socket):
        assert hub.socket is None
        with hub:
            assert isinstance(hub.socket, BrelSocket)
        assert hub.socket is None
        assert mocked_socket.closed

    def test_authenticate(self, hub):
        with hub:
            assert hub.socket.access_token is not None

    def test_no_key(self, hub):
        with raises(NoKeyException):
            with BrelHub("dummy"):
                pass

    def test_devices(self, hub, mocked_socket):
        mocked_socket.add_response(
            '{"msgType": "ReadDeviceAck", "mac": "abcdefgh001",'
            '"deviceType": "10000000", "msgID": "20210709110049549",'
            '"data": {"type": 1, "operation": 2, "currentPosition": 0,'
            '"currentAngle": 0, "currentState": 3, "voltageMode": 1,'
            '"batteryLevel": 1237, "wirelessMode": 1, "RSSI": -79}}'
        )
        with hub:
            devices = hub.devices
            assert len(devices) == 1  # The hub is not returned
            assert devices[0].mac == "abcdefgh001"
            assert devices[0]._device_type == "10000000"
        # Check devices are cached
        assert devices == hub.devices

    def test_get_access_token(self):
        """This example is provided in the documentation."""
        access_token = BrelHub._get_access_token(key="74ae544c-d16e-4c", token="37412C478E0FBEAB")
        assert access_token == "8570A96BC18ADB21D1FC155B24ECFD73"

    def test_mac(self, hub):
        with raises(NotConnectedException):
            _ = hub.mac
        with hub:
            assert hub.mac == "abcdefgh"
