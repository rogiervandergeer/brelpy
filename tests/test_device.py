from pytest import fixture

from brelpy import BrelDevice
from brelpy.socket import BrelSocket
from brelpy.types import BlindsType, LimitState, Operation, VoltageMode, WirelessMode


class TestDevice:
    @fixture(scope="function")
    def device(self, mocked_socket):
        socket = BrelSocket(host="dummy", port=1234)
        socket.access_token = "fake-token"
        mocked_socket.add_response(
            '{"msgType": "ReadDeviceAck", "mac": "abcdefgh001",'
            '"deviceType": "10000000", "msgID": "20210709110049549",'
            '"data": {"type": 1, "operation": 2, "currentPosition": 0,'
            '"currentAngle": 0, "currentState": 3, "voltageMode": 1,'
            '"batteryLevel": 1237, "wirelessMode": 1, "RSSI": -79}}'
        )
        mocked_socket.add_response(
            '{"msgType": "WriteDeviceAck", "mac": "abcdefgh001",'
            '"deviceType": "10000000", "msgID": "20210709110049549",'
            '"data": {"type": 1, "operation": 2, "currentPosition": 0,'
            '"currentAngle": 0, "currentState": 3, "voltageMode": 1,'
            '"batteryLevel": 1237, "wirelessMode": 1, "RSSI": -79}}'
        )
        return BrelDevice(mac="abcdefgh001", device_type="10000000", socket=socket)

    def test_repr(self, device):
        assert repr(device) == "<BrelDevice mac=abcdefgh001 device_type=10000000>"

    def test_angle(self, device, mocked_socket):
        assert device.angle == 0
        device.angle = 90
        assert '{"targetAngle": 90}' in mocked_socket.sent[-1]

    def test_battery(self, device):
        assert device.battery_level == 96
        assert device.battery_voltage == 12.37

    def test_close(self, device, mocked_socket):
        device.close()
        assert '{"operation": 0}' in mocked_socket.sent[-1]

    def test_down(self, device, mocked_socket):
        device.down()
        assert '{"operation": 0}' in mocked_socket.sent[-1]

    def test_limit_state(self, device):
        assert device.limit_state == LimitState.BOTH_LIMITS_DETECTED

    def test_open(self, device, mocked_socket):
        device.open()
        assert '{"operation": 1}' in mocked_socket.sent[-1]

    def test_operation(self, device):
        assert device.operation == Operation.STOPPED

    def test_position(self, device, mocked_socket):
        assert device.position == 0
        device.position = 100
        assert '{"targetPosition": 100}' in mocked_socket.sent[-1]

    def test_rssi(self, device):
        assert device.rssi == -79

    def test_stop(self, device, mocked_socket):
        device.stop()
        assert '{"operation": 2}' in mocked_socket.sent[-1]

    def test_type(self, device):
        assert device.type == BlindsType.VENETIAN_BLINDS

    def test_up(self, device, mocked_socket):
        device.up()
        assert '{"operation": 1}' in mocked_socket.sent[-1]

    def test_update(self, device, mocked_socket):
        device.update()
        assert '{"operation": 5}' in mocked_socket.sent[-1]

    def test_voltage_mode(self, device):
        assert device.voltage_mode == VoltageMode.DC_MOTOR

    def test_wireless_mode(self, device):
        assert device.wireless_mode == WirelessMode.BIDIRECTIONAL
