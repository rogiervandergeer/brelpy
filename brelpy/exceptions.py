class NoKeyException(Exception):
    pass


class NotConnectedException(Exception):
    pass
